# Summary

* [Introduction](README.md)

I started collecting sunflower seeds for planting in early April 2020. By mid-May, I had collected **32** packets of sunflowers. There were some duplicates and some other packets had multiple varieties of sunflower inside.

I planted the majority of the sunflower seeds between May 11, 2020 and  May 25, 2020. I (finally) stopped planting sunflower seeds in early July 2020. Among the first sunflower varieties to sprout were the Ms. Mars sunflowers. They first sprouted on Friday, May 29, 2020. By early July, the flowers were very well-formed and among the fastest growing of all. 